FROM atlas/analysisbase
COPY . /code
WORKDIR /code
RUN source ~/release_setup.sh && \
	sudo chown -R atlas /code && \
    g++ -o main main.cpp