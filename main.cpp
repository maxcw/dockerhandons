#include <fstream>
#include <iostream>
 
int main (int argc, char** argv)
{
   // open a file in write mode.
   std::ofstream outfile;
   std::cout << "Will write to file " << argv[1] << std::endl;
   outfile.open(argv[1]);

   // write inputted data into the file.
   outfile << "Testing docker handons with message" << argv[2] << std::endl;

   // close the opened file.
   outfile.close();
   
   return 0;
}